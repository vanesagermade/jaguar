/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.company.test;
import com.company.java.Puma;
import org.junit.Test;
import static org.junit.Assert.*;

public class PumaTest {

    @Test
    public void test(){
        //Instancia es igual a crear un objeto
        Puma yagua = new Puma(12, 120f);
        Puma yagua2 = new Puma();
        Puma yagua3 = new Puma(10);
        Puma yagua4 = new Puma(10f);
      
        yagua.come();
        yagua2.duerme();
        yagua3.rugir();
        yagua4.cazar();
    }

}



