/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.company.java;

public class Main {

    public static void main(String[] args) {


        int var = 4;

        //Instancia es igual a crear un objeto
        Jaguar yagua = new Jaguar(12, 120f);
        Jaguar yagua2 = new Jaguar();
        Jaguar yagua3 = new Jaguar(10);
        Jaguar yagua4 = new Jaguar(10f);

       // IFelino yagua5 = new Jaguar();



        System.out.println(yagua);
        System.out.println(yagua2);
        System.out.println(yagua3);
        System.out.println(yagua4);

        // System.out.println(yagua5);



        yagua.come();
        yagua.duerme();
        yagua.rugir();
        yagua.cazar();


    }
}
