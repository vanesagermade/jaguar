
/*
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.company.java;

public class Jaguar extends Animal implements IFelinoSalvaje{

    private int edad;
    private float peso;

    public Jaguar(){
        this.setPeso(0);
        this.setEdad(0);
    }

    public Jaguar(int edad){
        this.setEdad(edad);
        this.setPeso(0);
    }

    public Jaguar(float peso){
        this.setEdad(0);
        this.setPeso(peso);
    }

    public Jaguar(int edad, float peso){
        this.setPeso(peso);
        this.setEdad(edad);
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }
    public String toString(){
        return "edad:"+ this.getEdad() +" peso:"+ this.getPeso();
    }

    public void rugir(){
        System.out.println("rugir!");

    }
    public void cazar(){
        System.out.println("cazar!");
    }
   /* public void maullar(){
        throw new UnsupportedOperationException("El jaguar no maulla");
    }*/

    public void duerme(){
        System.out.println("el Jaguar duerme");
    }
}
